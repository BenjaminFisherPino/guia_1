#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from win_temp import win_temp


class MainWindow():

    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("window.ui")

#Ventana main
        win = builder.get_object("win")
        win.connect("destroy", Gtk.main_quit)
        win.set_title("Programa")
        win.resize(800, 600)

#Gtk entry
        str = builder.get_object("str")
        int = builder.get_object("int")

#Boton aceptar
        aceptar = builder.get_object("btn_aceptar")
        win_temp()
#Boton reset
        reset = builder.get_object("btn_reset")
        btn_reset()
#SHOW ALL
        win.show_all()

    def win_temp(self, btn = None, srt, int):
        cont = 0

        if cont == 0:
            str_temp = self.get_str()
            int_temp = self.get_int()
            cont = cont + 1

        else:
            str_final = str_temp + self.get_str()
            int_final = int_temp + self.get_int()

if __name__ == "__main__":
    MainWindow()
    Gtk.main()
