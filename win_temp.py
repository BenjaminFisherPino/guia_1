#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi
gi.require_version("Gtk","3.0")
from gi.repository import Gtk

class win_temp(self):

    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("window.ui")

        win_temp = builder.get_object("win_temp")
        win_temp-set_title("Ventana emergente")
        btn_aceptar = builder.get_object("btn_aceptar")
        btn_aceptar.connect("clicked", win_temp.destroy)
